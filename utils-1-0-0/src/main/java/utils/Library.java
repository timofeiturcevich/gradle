/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package utils;

import org.apache.commons.lang3.StringUtils;

public class Library {
    public static boolean isPositiveNumber(String str) {
        if(!StringUtils.isNumeric(str)){
            return false;
        }
        return Integer.parseInt(str)>=0;
    }
}
