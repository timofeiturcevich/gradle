/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package utils;

import org.junit.Test;
import static org.junit.Assert.*;

public class LibraryTest {
    @Test public void someLibraryMethodReturnsTrue() {
        assertTrue("someLibraryMethod should return 'true'", Library.isPositiveNumber("10"));
    }
}
